import Request from 'request'

class Client {

    get(uri) {
        return new Promise(async (resolve, reject) => {
            Request.get(uri, function (err, httpResponse, body) {
                if (err) {
                    reject(err);
                } else {
                    resolve(JSON.parse(body));
                }
            });
        });

    }

    post(uri, data) {
        return new Promise(async (resolve, reject) => {
            Request.post(uri, {form: data}, function (err, httpResponse, body) {
                if (err) {
                    reject(err);
                } else {
                    resolve(JSON.parse(body));
                }
            });
        });
    }

}

module.exports = new Client;