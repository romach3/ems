<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta property="og:title" content="МЧС Extremo"/>
    <meta property="og:image" content="http://ems.extremo.club/ems.png">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    @if(\Cookie::get('skin', 'light') === 'light')
        <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/flatly/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
    @else
        <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/slate/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
    @endif
    <link href="/css/app.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.15/fh-3.1.2/r-2.1.1/sc-1.4.2/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.15/fh-3.1.2/r-2.1.1/sc-1.4.2/datatables.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/locale/ru.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.15/dataRender/datetime.js"></script>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
    <script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
    <script src="/js/chartist-plugin-legend.js"></script>
    @if(\Gate::allows('edit'))
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css">
        <script src="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js"></script>
    @endif
    <style>
        .glyphicon {
            margin-right: 5px;
        }
        #app > .container {
            padding-bottom: 10px;
            padding-top: 10px;
            margin-bottom: 10px;
        }
        #app > .container.container-tr .breadcrumb {
            margin-bottom: 0;
        }
    @if(\Cookie::get('skin', 'light') === 'light')
        body {
            background-color: #f0f0f0 !important;
        }
        #app .container:not(.container-tr) {
            background-color: #fff !important;
            margin-bottom: 10px;
        }
    @else
        body {
           background-color: #32383e !important;
        }
        #app .container:not(.container-tr) {
            background-color: #272b30 !important;
            margin-bottom: 10px;
        }
        div.DTS tbody tr.odd {
            background-color: #353a41;
        }
        div.DTS tbody tr.even {
            background-color: inherit;
        }
    @endif
        div.DTS div.dataTables_scrollBody {
            background: transparent !important;
        }
        .ct-legend {
            position: relative;
            z-index: 10;
            list-style-type: none;
        }
        .ct-legend li {
            position: relative;
            padding-left: 23px;
            margin-bottom: 3px;
        }
        .ct-legend  li:before {
            width: 12px;
            height: 12px;
            position: absolute;
            left: 0;
            content: '';
            border: 3px solid transparent;
            border-radius: 2px;
            margin-top: 4px;
        }
        .ct-legend li.inactive:before {
            background: transparent;
        }
        .ct-legend .ct-legend-inside {
             position: absolute;
             top: 0;
             right: 0;
        }
        .ct-series-0:before {
            background-color: #f05b4f;
            border-color: #f05b4f;
        }
        .ct-series-1:before {
            background-color: #d70206;
            border-color: #d70206;
        }
        .ct-series-2:before {
            background-color: #f4c63d;
            border-color: #f4c63d;
        }
        .ct-line {
            stroke-width: 2px;
        }
        .ct-point {
            stroke-width: 5px;
        }
        .member {
            padding: 5px;
        }
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container container-tr">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{  url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        @if(\Auth::check())
                            <li><a href="{{ route('summary') }}">Сотрудники</a></li>
                            <li><a href="{{ route('logs.top') }}">Топ</a></li>
                           &nbsp;<li><a href="{{ route('logs.details') }}">Подбробный отчет</a></li>
                        @endif
                            @if(\Gate::allows('edit'))
                            <li><a href="{{ route('pages.all') }}">Страницы</a></li>
                        @endif
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        @if (Auth::check())
                            @if(\Cookie::get('skin', 'light') === 'light')
                                <li><a href="{{ route('skin', ['dark']) }}">Темный скин</a></li>
                            @else
                                <li><a href="{{ route('skin', ['light']) }}">Светлый скин</a></li>
                            @endif
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                            Выход
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
@if(\Auth::check())
    <script type="application/javascript">

    </script>
@endif

</body>
</html>
