@extends('layouts.app')

@section('content')
    <div class="container container-tr">
        <div class="row">
            <ul class="breadcrumb">
                <li><a href="{{ route('pages.all') }}">Страницы</a></li>
                <li class="active">{{ $row->title }}</li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" action="{{ route('pages.save') }}" method="POST">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" value="{{ $row->id }}">
                    <input type="hidden" name="name" value="{{ $row->name }}">
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Название</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="title" name="title" disabled required value="{{ $row->title }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="markdown" class="col-sm-2 control-label">Страница</label>
                        <div class="col-sm-10">
                            <textarea name="markdown" id="markdown">{{ $row->markdown }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <a href="{{ route('pages.all') }}" class="btn btn-default">Назад</a>
                            <button type="submit" class="btn btn-success">Сохранить</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <script type="application/javascript">
        $(function() {
            var simpleMDE = new SimpleMDE({ element: $("#markdown")[0] });
        });
    </script>
@endsection

