@extends('layouts.app')

@section('content')
    <div class="container container-tr">
        <div class="row">
            <ul class="breadcrumb">
                <li><a href="{{ route('pages.all') }}">Страницы</a></li>
                <li class="active">{{ $row->title }}</li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                {!! $row->html !!}
            </div>
        </div>
    </div>
@endsection

