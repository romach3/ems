@extends('layouts.app')

@section('content')
    <div class="container container-tr">
        <div class="row">
            <ul class="breadcrumb">
                <li class="active">Страницы</li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Страница</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($rows as $row)
                        <tr>
                            <td>{{ $row->title }}</td>
                            <td><a class="btn btn-success btn-sm" href="{{ route('pages.edit', [$row->name]) }}"><i class="glyphicon glyphicon-pencil"></i></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection