@extends('layouts.app')

@section('content')
    <div class="container container-tr">
        <div class="row">
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}">Список сотрудников</a></li>
                <li><a href="{{ route('logs.view', [$row->name]) }}">{{ $row->name }}</a></li>
                <li class="active">Редактирование</li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1>{{ $row->name }}</h1>
                </div>
                <form class="form-horizontal" action="{{ route('members.save') }}" method="POST">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" value="{{ $row->id }}">
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Имя</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" name="name" required value="{{ $row->name }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="steam_id" class="col-sm-2 control-label">Steam ID</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="steam_id" name="steam_id" required value="{{ $row->steam_id }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="rank" class="col-sm-2 control-label">Должность</label>
                        <div class="col-sm-10">
                            <select name="rank" required id="rank" class="form-control">
                                @foreach(\App\Member::$rank as $rank)
                                    <option value="{{ $rank }}" {{ $row->rank === $rank ? 'selected' : '' }}>{{ $rank }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="role" class="col-sm-2 control-label">Роль</label>
                        <div class="col-sm-10">
                            @if($row->isRegistered())
                                <select name="role" required id="rank" class="form-control">
                                    <option value="user" {{ $row->role === 'user' ? 'selected' : '' }}>Пользователь</option>
                                    <option value="admin" {{ $row->role === 'admin' ? 'selected' : '' }}>Администратор</option>
                                </select>
                            @else
                                <p class="text-danger">Пользователь ещё не авторизовался на сайте.</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="vacation" {{ $row->vacation ? 'checked' : '' }}> В отпуске
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="dismissed" {{ $row->dismissed ? 'checked' : '' }}> Уволен
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-sm-2 control-label">Комментарии</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="description" id="description">{{ array_get($row->metadata, 'description', '') }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <a href="{{ route('home') }}" class="btn btn-default">Назад</a>
                            <button type="submit" class="btn btn-success">Сохранить</button>
                            @if($row->id !== null)
                                <a href="{{ route('members.delete', [$row->id, csrf_token()]) }}" class="btn btn-danger pull-right">Удалить</a>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-sm-2 control-label"></label>
                        <div class="col-sm-10">
                            <hr>
                            @foreach(array_reverse(array_get($row->metadata, 'log', [])) as $log)
                                <p>{{ $log['time'] }}: {{ $log['message'] ??  $log['messagee'] }}</p>
                            @endforeach
                        </div>
                    </div>
                </form>
            </div>
            @if($row->id === null)
            <div class="col-md-12">
                <hr>
                <h3>Переименовать</h3>
                <form class="form-horizontal" action="{{ route('members.rename') }}" method="POST">
                    {!! csrf_field() !!}
                    <input type="hidden" name="last_name" value="{{ $row->name }}">
                    <div class="form-group">
                        <label for="rank" class="col-sm-2 control-label">Имя</label>
                        <div class="col-sm-10">
                            <select name="new_name" required id="name" class="form-control">
                                @foreach((new \App\Member())->where('dismissed', false)->orderBy('name')->get() as $row)
                                    <option value="{{ $row->name }}">{{ $row->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success">Переименовать</button>
                        </div>
                    </div>
                </form>
            </div>
            @endif
        </div>
    </div>
@endsection

