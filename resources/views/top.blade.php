@extends('layouts.app')

@section('content')
    <div class="container container-tr">
        <div class="row">
            <ul class="breadcrumb">
                <li><a href="{{ route('summary') }}">Список сотрудников</a></li>
                <li class="active">Топ</li>
            </ul>
        </div>
    </div>

    <div class="container container__block">
        <div class="row">
            <div class="col-xs-12">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#currentMonth" role="tab" data-toggle="tab">Топ онлайна за текущий месяц</a></li>
                    <li><a href="#lastMonth" role="tab" data-toggle="tab">Топ онлайна за прошлый месяц</a></li>
                    <li><a href="#currentWeek" role="tab" data-toggle="tab">Топ онлайна за текущую неделю</a></li>
                    <li><a href="#lastWeek" role="tab" data-toggle="tab">Топ онлайна за прошлую неделю</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="currentMonth">
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <h4>Алтис</h4>
                                <ul class="list-group">
                                    @foreach($month['current']['altis'] as $key => $row)
                                        <li class="list-group-item">
                                            <span class="badge">{{ diffInHours($row->session_time_sum) }}</span>
                                            <a href="{{ route('logs.view', [$row->name]) }}">{{ $row->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <h4>Малден</h4>
                                <ul class="list-group">
                                    @foreach($month['current']['malden'] as $key => $row)
                                        <li class="list-group-item">
                                            <span class="badge">{{ diffInHours($row->session_time_sum) }}</span>
                                            <a href="{{ route('logs.view', [$row->name]) }}">{{ $row->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <h4>Таноа</h4>
                                <ul class="list-group">
                                    @foreach($month['current']['tanoa'] as $key => $row)
                                        <li class="list-group-item">
                                            <span class="badge">{{ diffInHours($row->session_time_sum) }}</span>
                                            <a href="{{ route('logs.view', [$row->name]) }}">{{ $row->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lastMonth">
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <h4>Алтис</h4>
                                <ul class="list-group">
                                    @foreach($month['last']['altis'] as $key => $row)
                                        <li class="list-group-item">
                                            <span class="badge">{{ diffInHours($row->session_time_sum) }}</span>
                                            <a href="{{ route('logs.view', [$row->name]) }}">{{ $row->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <h4>Малден</h4>
                                <ul class="list-group">
                                    @foreach($month['last']['malden'] as $key => $row)
                                        <li class="list-group-item">
                                            <span class="badge">{{ diffInHours($row->session_time_sum) }}</span>
                                            <a href="{{ route('logs.view', [$row->name]) }}">{{ $row->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <h4>Таноа</h4>
                                <ul class="list-group">
                                    @foreach($month['last']['tanoa'] as $key => $row)
                                        <li class="list-group-item">
                                            <span class="badge">{{ diffInHours($row->session_time_sum) }}</span>
                                            <a href="{{ route('logs.view', [$row->name]) }}">{{ $row->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="currentWeek">
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <h4>Алтис</h4>
                                <ul class="list-group">
                                    @foreach($week['current']['altis'] as $key => $row)
                                        <li class="list-group-item">
                                            <span class="badge">{{ diffInHours($row->session_time_sum) }}</span>
                                            <a href="{{ route('logs.view', [$row->name]) }}">{{ $row->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <h4>Малден</h4>
                                <ul class="list-group">
                                    @foreach($week['current']['malden'] as $key => $row)
                                        <li class="list-group-item">
                                            <span class="badge">{{ diffInHours($row->session_time_sum) }}</span>
                                            <a href="{{ route('logs.view', [$row->name]) }}">{{ $row->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <h4>Таноа</h4>
                                <ul class="list-group">
                                    @foreach($week['current']['tanoa'] as $key => $row)
                                        <li class="list-group-item">
                                            <span class="badge">{{ diffInHours($row->session_time_sum) }}</span>
                                            <a href="{{ route('logs.view', [$row->name]) }}">{{ $row->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lastWeek">
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <h4>Алтис</h4>
                                <ul class="list-group">
                                    @foreach($week['last']['altis'] as $key => $row)
                                        <li class="list-group-item">
                                            <span class="badge">{{ diffInHours($row->session_time_sum) }}</span>
                                            <a href="{{ route('logs.view', [$row->name]) }}">{{ $row->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <h4>Малден</h4>
                                <ul class="list-group">
                                    @foreach($week['last']['malden'] as $key => $row)
                                        <li class="list-group-item">
                                            <span class="badge">{{ diffInHours($row->session_time_sum) }}</span>
                                            <a href="{{ route('logs.view', [$row->name]) }}">{{ $row->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <h4>Таноа</h4>
                                <ul class="list-group">
                                    @foreach($week['last']['tanoa'] as $key => $row)
                                        <li class="list-group-item">
                                            <span class="badge">{{ diffInHours($row->session_time_sum) }}</span>
                                            <a href="{{ route('logs.view', [$row->name]) }}">{{ $row->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container container__block">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-xs-12">
                        <h3>Онлайн по часам</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12" style="height: 320px">
                        <div class="hours-chart-all" style="height: 250px"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="application/javascript">
        $(function() {
            new Chartist.Line('.hours-chart-all', {
                labels: JSON.parse('{{ json_encode(range(0, 23)) }}'),
                series: [
                    JSON.parse('{{ json_encode(array_values($hoursAll)) }}'),
                    JSON.parse('{{ json_encode(array_values($hoursWork)) }}'),
                    JSON.parse('{{ json_encode(array_values($hoursHoliday)) }}')
                ]
            }, {
                fullWidth: true,
                showArea: true,
                low: 0,
                chartPadding: {
                    right: 40
                },
                plugins: [
                    Chartist.plugins.legend({
                        legendNames: ['Всего', 'В будни', 'В выходные']
                    })
                ]
            });
        });
    </script>
@endsection

