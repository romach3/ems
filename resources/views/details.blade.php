@extends('layouts.app')

@section('content')
    <div class="container container-tr">
        <div class="row">
            <ul class="breadcrumb">
                <li><a href="{{ route('summary') }}">Список сотрудников</a></li>
                <li class="active">Подробный отчет</li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered" id="logs-table">
                    <thead>
                    <tr>
                        <th>Имя</th>
                        <th>Сервер</th>
                        <th>Время сессии</th>
                        <th>Время входа</th>
                        <th>Время выхода</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <script type="application/javascript">
        $(function() {
            var height = window.innerHeight - $('#logs-table').offset().top - 150;
            if (height < 200) {
                height = 200;
            }
            $('#logs-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('logs.data') !!}',
                scrollY:        height,
                deferRender:    true,
                scroller:       true,
                ordering: false,
                columns: [
                    { data: 'name', name: 'name' },
                    { data: 'server', name: 'server' },
                    { data: 'session_time_human', name: 'session_time_human' },
                    { data: 'join_at', name: 'join_at' },
                    { data: 'created_at', name: 'created_at' },
                ],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Russian.json"
                },
                columnDefs: [ {
                    targets: 4,
                    render: $.fn.dataTable.render.moment( 'Y-MM-DD HH:mm:ss', 'DD MMMM в HH:mm', 'ru' )
                }, {
                    targets: 3,
                    render: $.fn.dataTable.render.moment( 'Y-MM-DD HH:mm:ss', 'DD MMMM в HH:mm', 'ru' )
                } ]
            });
        });
    </script>
@endsection

