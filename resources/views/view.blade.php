@extends('layouts.app')

@section('content')
<div class="container container-tr">
    <div class="row">
        <ul class="breadcrumb">
            <li><a href="{{ route('summary') }}">Список сотрудников</a></li>
            <li class="active">{{ $name }}</li>
        </ul>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>{{ $name }}
                    @if(\Gate::allows('edit'))
                        <small><a href="{{ route('members.edit', $name) }}"><i class="glyphicon glyphicon-pencil"></i></a> </small>
                    @endif
                </h1>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <h4>Всего часов:</h4>
                    <ul>
                        @foreach($statistics['total']['servers'] as $row)
                            <li>{{ $row->server }}: {{ diffInHours($row->session_time_sum) }}</li>
                        @endforeach
                        <li><strong>Всего: {{ diffInHours($statistics['total']['total']) }}</strong></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-md-4">
                    <h4>За текущий месяц:</h4>
                    <ul>
                        @foreach($statistics['currentMonth']['servers'] as $row)
                            <li>{{ $row->server }}: {{ diffInHours($row->session_time_sum) }}</li>
                        @endforeach
                        <li><strong>Всего: {{ diffInHours($statistics['currentMonth']['total']) }}</strong></li>
                        @if($statistics['currentMonth']['retention'])
                            <li class="text-success">Должность удержана</li>
                        @endif
                        @if($statistics['currentMonth']['up'])
                            <li class="text-success">Готов к повышению</li>
                        @endif
                    </ul>
                </div>
                <div class="col-xs-12 col-md-4">
                    <h4>За прошлый месяц:</h4>
                    <ul>
                        @foreach($statistics['lastMonth']['servers'] as $row)
                            <li>{{ $row->server }}: {{ diffInHours($row->session_time_sum) }}</li>
                        @endforeach
                        <li><strong>Всего: {{ diffInHours($statistics['lastMonth']['total']) }}</strong></li>
                        @if($statistics['lastMonth']['retention'])
                            <li class="text-success">Должность удержана</li>
                        @endif
                        @if($statistics['lastMonth']['up'])
                            <li class="text-success">Готов к повышению</li>
                        @endif
                    </ul>
                </div>
            </div>

            <h2>Лог</h2>
            <table class="table table-bordered" id="logs-table">
                <thead>
                <tr>
                    <th>Имя</th>
                    <th>Сервер</th>
                    <th>Время сессии</th>
                    <th>Время входа</th>
                    <th>Время выхода</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


<script type="application/javascript">
    $(function() {
        var height = window.innerHeight - $('#logs-table').offset().top - 150;
        if (height < 200) {
            height = 400;
        }
        $('#logs-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('logs.data', [$name]) !!}',
            scrollY:        height,
            deferRender:    true,
            scroller:       true,
            searching: false,
            ordering: false,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Russian.json"
            },
            columns: [
                { data: 'name', name: 'name' },
                { data: 'server', name: 'server' },
                { data: 'session_time_human', name: 'session_time_human' },
                { data: 'join_at', name: 'join_at' },
                { data: 'created_at', name: 'created_at' },
            ],
            columnDefs: [ {
                targets: 4,
                render: $.fn.dataTable.render.moment( 'Y-MM-DD HH:mm:ss', 'DD MMMM в HH:mm', 'ru' )
            }, {
                targets: 3,
                render: $.fn.dataTable.render.moment( 'Y-MM-DD HH:mm:ss', 'DD MMMM в HH:mm', 'ru' )
            } ]
        });
    });
</script>
@endsection

