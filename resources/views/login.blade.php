@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 ">
            @if(\Auth::check())
                <div class="container container__block">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3>Игроки онлайн</h3>
                        </div>
                        @foreach($servers as $server)
                            <div class="col-xs-12">
                                <h4>{{ $server['name'] }} (Всего игроков: {{ $server['total_all'] }}, сотрудников: {{ $server['total_ems'] }})</h4>
                                <p>
                                    @foreach($server['items'] as $item)
                                        @if($item['group'] === 'ems')
                                            <span class="member text-danger">{{ $item['name'] }}</span>
                                        @else
                                            <span class="member">{{ $item['name'] }}</span>
                                        @endif
                                    @endforeach
                                </p>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
            <div class="container container__block">
                <div class="row">
                    <div class="col-md-12">
                        {!! (new \App\Page())->where('name', 'for_guests')->firstOrFail()->html !!}
                    </div>
                </div>
            </div>
            @if(\Auth::check())
                <div class="container container__block">
                    <div class="row">
                        <div class="col-md-12">
                            {!! (new \App\Page())->where('name', 'for_customers')->firstOrFail()->html !!}
                        </div>
                    </div>
                </div>
            @else
                <div class="container container__block">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Вход на сайт для сотрудников МЧС Extremo</h3>
                            <hr>
                            <p class="text-center">
                                <a href="{{ route('login') }}">
                                    <img src="http://cdn.steamcommunity.com/public/images/signinthroughsteam/sits_large_border.png">
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>

@endsection

