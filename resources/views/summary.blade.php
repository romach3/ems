@extends('layouts.app')

@section('content')
<div class="container container-tr">
    <div class="row">
        <ul class="breadcrumb">
            <li>Список сотрудников</li>
            @if($month === 'last')
                <li><a href="{{ route('summary', ['month' => 'current']) }}">Прошлый месяц</a></li>
            @else
                <li><a href="{{ route('summary', ['month' => 'last']) }}">Текущий месяц</a></li>
            @endif
        </ul>
    </div>
</div>
<div class="container">
    <div class="row">
        @if(\Gate::allows('edit') && count($notExists) > 0)
            <div class="col-md-12">
                <div class="alert alert-dismissible alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Новые сотрудники:</strong>
                    @foreach($notExists as $row)
                        <a href="{{ route('members.edit', [$row]) }}" class="alert-link">{{ $row }}</a>
                    @endforeach
                </div>
            </div>
        @endif
        <div class="col-md-12">
            <table class="table table-bordered" id="logs-table">
                <thead>
                <tr>
                    <th>Имя</th>
                    <th>Должность</th>
                    <th>Алтис</th>
                    <th>Таноа</th>
                    <th>Малден</th>
                    <th>Всего</th>
                </tr>
                </thead>
                <tbody>
                @foreach($rows as $row)
                    <tr>
                        <td>
                            <a href="{{ route('logs.view', [$row['name']]) }}">{{ $row['name'] }}</a>
                            @if($row['retention'])
                                <i class="glyphicon glyphicon-thumbs-up text-success pull-right" title="Должность удержана"></i>
                            @endif
                            @if($row['up'])
                                <i class="glyphicon glyphicon-chevron-up text-success pull-right" title="Готов к повышению"></i>
                            @endif
                            @if($row['dismissed'])
                                <i class="glyphicon glyphicon-remove-sign text-danger pull-right" title="Уволен"></i>
                            @endif
                        </td>
                        <td>{{ $row['rank'] }}</td>
                        <td>{{ isset($row['Алтис']) ? diffInHours($row['Алтис']): '-' }}</td>
                        <td>{{ isset($row['Таноа']) ? diffInHours($row['Таноа']): '-' }}</td>
                        <td>{{ isset($row['Малден']) ? diffInHours($row['Малден']): '-' }}</td>
                        <td>{{ diffInHours($row['total']) }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="application/javascript">
    $(function() {
        $('#logs-table').DataTable({
            deferRender:    true,
            ordering: false,
            paging: false,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Russian.json"
            },
        });
    });
</script>
@endsection

