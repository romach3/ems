    <?php

    $returnUri = 'http://forum.extremo.club/get_steam_id.php?response';
    $forumUri = 'http://forum.extremo.club';

    if (isset($_GET['request'])) {
        $params = array(
            'openid.ns'         => 'http://specs.openid.net/auth/2.0',
            'openid.mode'       => 'checkid_setup',
            'openid.return_to'  => $returnUri.'&'.http_build_query([
                    'display_name'  => isset($_GET['display_name']) ? $_GET['display_name'] : '',
                    'email_1'       => isset($_GET['email_1']) ? $_GET['email_1'] : '',
                ], '', '&'),
            'openid.realm'      => 'http://' . $_SERVER['HTTP_HOST'],
            'openid.identity'   => 'http://specs.openid.net/auth/2.0/identifier_select',
            'openid.claimed_id' => 'http://specs.openid.net/auth/2.0/identifier_select',
        );
        header('Location: https://steamcommunity.com/openid/login?' . http_build_query($params, '', '&'));
        exit;
    } elseif (isset($_GET['response'])) {
        $uri = explode('/id/', $_GET['openid_identity']);
        $id = $uri[1];
        header('Location: '.$forumUri.'/index.php?/register/&core_pfield_1='.$id.'&'.http_build_query([
                'username'  => isset($_GET['display_name']) ? $_GET['display_name'] : '',
                'email_address'       => isset($_GET['email_1']) ? $_GET['email_1'] : '',
            ], '', '&'));
        exit;
    } else {
        header("HTTP/1.0 404 Not Found");
        exit;
    }
