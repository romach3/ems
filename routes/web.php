<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();
//AuthController@login
Route::get('login', 'AuthController@login')->name('login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('userNotFound', 'AuthController@userNotFound')->name('userNotFound');

Route::get('/', 'HomeController@check')->name('home');

Route::get('/summary', 'AppController@summary')->name('summary');
Route::get('/data/{name?}', 'AppController@data')->name('logs.data');
Route::get('/view/{name}', 'AppController@view')->name('logs.view');
Route::get('/details', 'AppController@details')->name('logs.details');
Route::get('/top', 'AppController@top')->name('logs.top');
Route::get('/up', 'AppController@up')->name('logs.up');
Route::get('/skin/{type}', 'AppController@skin')->name('skin');

Route::get('/members/edit/{name}', 'MembersController@edit')->name('members.edit');
Route::post('/members/save', 'MembersController@save')->name('members.save');
Route::get('/members/delete/{id}/{token}', 'MembersController@delete')->name('members.delete');
Route::post('/members/rename', 'MembersController@rename')->name('members.rename');

Route::get('/pages/edit/{name}', 'PagesController@edit')->name('pages.edit');
Route::post('/pages/save', 'PagesController@save')->name('pages.save');
Route::get('/pages/all', 'PagesController@all')->name('pages.all');
Route::get('/p/{name}', 'AppController@page')->name('pages.view');
Route::get('/vue', 'AppController@vue')->name('vue.index');

Route::get('/steam/request', 'HomeController@steamRequest');
Route::get('/steam/response', 'HomeController@steamResponse');










