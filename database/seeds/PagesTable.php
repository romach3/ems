<?php

use Illuminate\Database\Seeder;

class PagesTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            'name' => 'for_customers',
            'markdown' => '',
            'html' => '',
        ]);
        DB::table('pages')->insert([
            'name' => 'for_guests',
            'markdown' => '',
            'html' => '',
        ]);
    }
}
