<?php

namespace App\Http\Controllers;

use App\EMS;
use App\Member;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MembersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('can:edit');
    }

    public function edit(Request $request, Member $member, $name) {
        $row = $member->where('name', $name)->firstOrNew([]);
        $row->name = $name;
        return view('members.edit', ['row' => $row]);
    }

    public function save(Request $request, Member $member) {
        $this->validate($request, [
            'name' => 'required',
            'rank' => 'required',
            'steam_id' => 'required',
        ]);
        $row = $member->findOrNew((int) $request->get('id', 0));
        $metadata = $row->metadata;
        if ($row->id === null) {
            $this->log($metadata, 'Сотрудник зарегистрирован.');
        }
        $row->name = $request->get('name');
        if ($row->id !== null && $row->rank !== $request->get('rank')) {
            $this->log($metadata, 'Должность изменена c '.$row->rank.' на '.$request->get('rank'));
        }
        $row->rank = $request->get('rank');
        if ($row->id !== null && $row->vacation !== (bool) $request->get('vacation', false)) {
            if ($request->get('vacation', false)) {
                $this->log($metadata, 'Сотрудник ушел в отпуск.');
            } else {
                $this->log($metadata, 'Сотрудник вернулся из отпуска.');
            }
        }
        $row->vacation = (bool) $request->get('vacation', false);
        if ($row->id !== null && $row->dismissed !== (bool) $request->get('dismissed', false)) {
            if ($request->get('dismissed', false)) {
                $this->log($metadata, 'Сотрудник уволен.');
            } else {
                $this->log($metadata, 'Сотрудник восстановлен в должности.');
            }
        }
        $row->dismissed = (bool) $request->get('dismissed', false);
        $metadata['description'] = $request->get('description', '');
        $row->metadata = $metadata;
        $user = (new User())->where('steam_id', $row->steam_id)->first();
        $row->steam_id = $request->get('steam_id');
        if ($user !== null) {
            $this->validate($request, [
                'role' => 'in:user,admin'
            ]);
            $user->role = $request->get('role', 'user');
            $user->steam_id = $request->get('steam_id');
            $user->save();
        }
        $row->save();
        return redirect()->route('logs.view', [$row->name]);
    }

    public function rename(Request $request, EMS $EMS) {
        $this->validate($request, [
            'last_name' => 'required',
            'new_name' => 'required'
        ]);
        $EMS->where('name', $request->get('last_name'))->update([
            'name' => $request->get('new_name')
        ]);
        return redirect()->route('summary');
    }

    public function delete($id, $token) {
        if ($token !== csrf_token()) {
            abort(404);
        }
        $member = (new Member())->find($id);
        (new User())->where('steam_id', $member->steam_id)->delete();
        $member->delete();
        return redirect()->route('summary');
    }

    protected function log(&$metadata, $message) {
        if (!isset($metadata['log'])) {
            $metadata['log'] = [];
        }
        $metadata['log'][] = ['time' => (new Carbon())->toDateTimeString(), 'message'  => $message];
    }

}
