<?php namespace App\Http\Controllers;

use App\Member;
use App\User;
use Invisnik\LaravelSteamAuth\SteamAuth;
use Auth;

class AuthController extends Controller
{
    /**
     * @var SteamAuth
     */
    private $steam;

    public function __construct(SteamAuth $steam)
    {
        $this->steam = $steam;
    }

    public function login()
    {
        if ($this->steam->validate()) {
            $info = $this->steam->getUserInfo();
            if (!is_null($info)) {
                $user = (new User())->where('steam_id', $info->steamID64)->first();
                if (is_null($user)) {
                    $member = (new Member())->where('steam_id', $info->steamID64)->first();
                    if ($member === null || $member->dismissed) {
                        return redirect()->route('userNotFound');
                    }
                    $user = (new User())->create([
                        'name' => $member->name,
                        'username' => $info->personaname,
                        'avatar'   => $info->avatarfull,
                        'steam_id'  => $info->steamID64,
                        'email' => $info->steamID64.'@example.com',
                        'password' => bcrypt(str_random()),
                    ]);
                }
                Auth::login($user, true);
                return redirect()->route('logs.view', $user->name); // redirect to site
            }
        }
        return $this->steam->redirect(); // redirect to Steam login page
    }

    public function userNotFound() {
        return view('auth.user_not_found');
    }
}