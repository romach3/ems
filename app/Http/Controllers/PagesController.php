<?php namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;
use GrahamCampbell\Markdown\Facades\Markdown;

class PagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:edit');
    }

    public function edit(Request $request, $name) {
        return view('pages.edit', [
            'row' => (new Page())->where('name', $name)->firstOrFail()
        ]);
    }

    public function save(Request $request) {
        $this->validate($request, [
            'id' => 'required|exists:pages,id',
            'name' => 'required|exists:pages,name',
            'markdown' => 'required',
        ]);
        $page = (new Page())->findOrFail($request->get('id'));
        $page->markdown = $request->get('markdown');
        $page->html = Markdown::convertToHtml($request->get('markdown'));
        $page->save();
        return redirect()->back();
    }

    public function all(Request $request) {
        return view('pages.all', [
            'rows' => (new Page())->all()
        ]);
    }

}
