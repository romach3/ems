<?php

namespace App\Http\Controllers;

use App\EMS;
use App\Member;
use App\Page;
use App\Services\Online\Online;
use App\Services\Statistics;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AppController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        return view('details');
    }

    public function data($name = null)
    {
        $query = (new EMS)
            ->where('vacation', false)
            ->where('action', 0)
            ->where('session_time', '>=', 60)
            ->orderBy('created_at', 'desc');
        if ($name !== null) {
            $query = $query->where('name', $name);
        }
        return \Datatables::of($query)
            ->addColumn('join_at', function(EMS $ems) {
                /** @var Carbon $created_at */
                $created_at = $ems->created_at;
                return $created_at->subSeconds($ems->session_time);
            })
            ->addColumn('session_time_human', function(EMS $ems) {
                /** @var Carbon $created_at */
                $created_at = $ems->created_at;
                /** @var Carbon $join_at */
                $join_at = (clone $created_at)->subSeconds($ems->session_time);
                $diff = $join_at->diff($created_at);
                return $diff->format('%h ч. %I мин.');
            })
            ->editColumn('name', function(EMS $ems) {
                $uri = route('logs.view', [$ems->name]);
                return "<a href='$uri'>$ems->name</a>";
            })
            ->escapeColumns([])
            ->whitelist(['name'])
            ->make(true);
    }

    public function view($name) {
        $member = (new Member())->where('name', $name)->first();
        $statistics = new Statistics\Member($name, $member !== null ? $member->rank : false);
        return view('view', ['name' => $name, 'statistics' => [
            'total' => $statistics->total(),
            'lastMonth' => $statistics->lastMonth(),
            'currentMonth' => $statistics->currentMonth()
        ]]);
    }

    public function summary(Request $request) {
        if (\Gate::allows('edit')) {
            $existsInLog = (new EMS)->distinct()->where('created_at', '>', (new Carbon())->subWeek(2))->pluck('name')->toArray();
            $existsInMembers = (new Member())->distinct()->pluck('name')->toArray();
            $notExists = array_diff($existsInLog, $existsInMembers);
        }
        $response = $this->getSummary($request->get('month', 'current'));
        return view('summary', ['rows' => $response, 'notExists' => $notExists ?? [], 'month' => $request->get('month', 'current')]);
    }

    public function up() {
        $summary = $this->getSummary();
        return view('up');
    }

    public function top() {
        return view('top', [
            'month' => [
                'last' => [
                    'altis' => (new Statistics\Top)->lastMonth(3),
                    'malden' => (new Statistics\Top)->lastMonth(4),
                    'tanoa' => (new Statistics\Top)->lastMonth(106),
                ],
                'current' => [
                    'altis' => (new Statistics\Top)->currentMonth(3),
                    'malden' => (new Statistics\Top)->currentMonth(4),
                    'tanoa' => (new Statistics\Top)->currentMonth(106),
                ]
            ],
            'week' => [
                'last' => [
                    'altis' => (new Statistics\Top)->lastWeek(3),
                    'malden' => (new Statistics\Top)->lastWeek(4),
                    'tanoa' => (new Statistics\Top)->lastWeek(106),
                ],
                'current' => [
                    'altis' => (new Statistics\Top)->currentWeek(3),
                    'malden' => (new Statistics\Top)->currentWeek(4),
                    'tanoa' => (new Statistics\Top)->currentWeek(106),
                ]
            ],
            'hoursWork' => (new Statistics\Top())->hours('work'),
            'hoursHoliday' => (new Statistics\Top())->hours('holiday'),
            'hoursAll' => (new Statistics\Top())->hours('all'),
        ]);
    }

    public function skin($type) {
        if (!in_array($type, ['dark', 'light'])) {
            $type = 'light';
        }
        \Cookie::queue('skin', $type);
        return back();
    }

    protected function getSummary($month = 'current') {
        $query = (new EMS())->select(['name', 'server', \DB::raw('sum(session_time) AS session_time_sum')])
            ->where('vacation', false)
            ->where('session_time', '>=', 60)
            ->groupBy(['name', 'server'])->orderBy('name')->orderBy('server');
        if ($month === 'last') {
            $query = $query->where('created_at', '<', (new Carbon())->startOfMonth())
                ->where('created_at', '>=', (new Carbon())->subMonth()->startOfMonth());
        } else {
            $query = $query->where('created_at', '>=', (new Carbon())->startOfMonth());
        }
        $rows = $query->get();
        $response = [];
        $members = (new Member())->distinct()->pluck('rank', 'name')->toArray();
        $dismissed = (new Member())->distinct()->pluck('dismissed', 'name')->toArray();
        foreach ($rows as $row) {
            if (isset($dismissed[$row->name]) && $dismissed[$row->name] && !\Gate::allows('edit')) {
                continue;
            }
            if (!isset($response[$row->name])) {
                $response[$row->name] = [
                    'name' => $row->name, 'total' => 0, 'rank' => $members[$row->name] ?? 'new',
                    'dismissed' => isset($dismissed[$row->name]) ? $dismissed[$row->name] : false,
                ];
            }
            if (!isset($response[$row->name][$row->server])) {
                $response[$row->name][$row->server] = 0;
            }
            $response[$row->name][$row->server] += $row->session_time_sum;
            $response[$row->name]['total'] += $row->session_time_sum;
        }
        foreach ($response as &$row) {
            $row['up'] = (new Statistics\Member($row['name']))->isReadyToUp($row['rank'], $row['total']);
            $row['retention'] = (new Statistics\Member($row['name']))->isRetention($row['rank'], $row['total']);
        }
        $response = array_sort($response, function($row) {
            return $row['dismissed'] ? 1000 : array_search($row['rank'], array_reverse(Member::$rank));
        });
        return $response;
    }

    public function page($name) {
        return view('pages.view', [
            'row' => (new Page())->where('name', $name)->firstOrFail()
        ]);
    }

    public function vue() {
        return file_get_contents('C:\Users\Administrator\AppData\Roaming\TS3Client\plugins\lua_plugin\Modules_EMSCore\Logs\3.log');
    }
}
