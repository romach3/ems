<?php

namespace App\Http\Controllers;

use App\EMS;
use App\Member;
use App\Services\Online\Online;
use App\Services\Statistics;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function check() {
        if (\Auth::check()) {
            return view('login', ['servers' => (new Online())->create()->get()]);
        } else {
            return view('login');
        }
    }

    public function steamRequest(Request $request) {
        $return = 'http://ems.extremo.club/steam/response';
        $params = array(
            'openid.ns'         => 'http://specs.openid.net/auth/2.0',
            'openid.mode'       => 'checkid_setup',
            'openid.return_to'  => $return,
            'openid.realm'      => (\Config::get('steam-auth.https') ? 'https' : 'http') . '://' . $request->server('HTTP_HOST'),
            'openid.identity'   => 'http://specs.openid.net/auth/2.0/identifier_select',
            'openid.claimed_id' => 'http://specs.openid.net/auth/2.0/identifier_select',
        );
        return redirect()->to('https://steamcommunity.com/openid/login?' . http_build_query($params, '', '&'));
    }

    public function steamResponse(Request $request) {
        $uri = $request->get('openid_identity');
        $id = explode('/id/', $uri)[1];
        $client = new Client();
        $response = $client->get('http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key='.env('STEAM_API_KEY').'&steamids='.$id)
            ->getBody()->getContents();
        return redirect()->to('http://forum.extremo.club/index.php?app=core&module=global&section=register&field_1='.$id);
    }

}
