<?php

namespace App\Console\Commands;

use App\EMS;
use App\Member;
use Carbon\Carbon;
use Illuminate\Console\Command;

class LogParser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'log:parser';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $vacation = [];
        if (is_writable(env('LOG_FILE'))) {
            \File::move(env('LOG_FILE'), storage_path('log.txt'));
            $file = file_get_contents(storage_path('log.txt'));
            $lines = explode(PHP_EOL, $file);
            foreach ($lines as $line) {
                if (empty(trim($line))) {
                    continue;
                }
                $raw = explode('] ', $line);
                $cols = explode('|', $raw[1]);
                $row = new EMS();
                $row->timestamps = false;
                $row->created_at = Carbon::createFromFormat('Y/m/d H:i:s', str_replace(['[', ']'], '', $raw[0]));
                $row->updated_at = $row->created_at;
                $row->name = $cols[0];
                $row->action = $cols[1];
                $row->server = $cols[2];
                $row->session_time = $cols[3] ?? 0;
                if (!isset($vacation[$row->name])) {
                    $member = (new Member())->where('name', $row->name)->first();
                    if ($member !== null) {
                        $vacation[$member->name] = $member->vacation;
                    } else {
                        $vacation[$row->name] = false;
                    }
                }
                $row->vacation = $vacation[$row->name];
                $row->save();
            }
            \File::delete(storage_path('log.txt'));
        }
    }
}
