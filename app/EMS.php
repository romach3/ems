<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EMS extends Model
{
    protected $table = 'ems_stat';
    public static $servers = [3 => 'Алтис', 4 => 'Малден', 106 => 'Таноа'];


    public function getServerAttribute($value) {
        return self::$servers[$value];
    }
}
