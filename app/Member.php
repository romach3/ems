<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{

    protected $casts = [
        'metadata' => 'array'
    ];

    static $rank = [
        'Интерн',
        'Техник',
        'Специалист',
        'Старший специалист',
        'Мастер',
        'Зам. министра',
        'Министр'
    ];

    static $up = [
        'Интерн' => 90,
        'Техник' => 80,
        'Специалист' => 70,
        'Старший специалист' => 60,
        'Мастер' => false,
        'Зам. министра' => false,
        'Министр' => false
    ];

    static $retention = [
        'Интерн' => false,
        'Техник' => false,
        'Специалист' => 50,
        'Старший специалист' => 40,
        'Мастер' => 30,
        'Зам. министра' => false,
        'Министр' => false
    ];

    protected $selfRole = null;

    public function isRegistered() {
        return (new User())->where('steam_id', $this->steam_id)->exists();
    }

    public function getRoleAttribute() {
        if ($this->selfRole !== null) {
            return $this->selfRole;
        }
        $user = (new User())->where('steam_id', $this->steam_id)->first();
        if ($user === null) {
            return $user;
        } else {
            $this->selfRole = $user->role;
            return $user->role;
        }
    }
}
