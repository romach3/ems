<?php

function diffInHours(int $seconds) {
    $hours = floor($seconds / 60 / 60);
    $minutes = ceil(($seconds - ($hours * 60 * 60)) / 60);
    return sprintf('%1$01d ч. %2$02d мин.', $hours, $minutes);
}