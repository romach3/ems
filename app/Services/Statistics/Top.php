<?php namespace App\Services\Statistics;

use App\EMS;
use Carbon\Carbon;

class Top
{
    public function server(int $name, int $period)
    {
        return (new EMS)->where('ems_stat.server', $name)
            ->leftJoin('members', 'members.name', '=', 'ems_stat.name')
            ->where('members.dismissed', false)
            ->where('ems_stat.created_at', '>', (new Carbon())->subDays($period))
            ->select(['ems_stat.name', \DB::raw('sum(ems_stat.session_time) AS session_time_sum')])
            ->groupBy('ems_stat.name')->orderBy('session_time_sum', 'desc')->limit(10)->get();
    }

    public function currentMonth(int $server)
    {
        return $this->getTopQuery($server)
            ->where('ems_stat.created_at', '>', (new Carbon())->startOfMonth())
            ->where('ems_stat.created_at', '<=', (new Carbon())->endOfMonth())->get();
    }

    public function lastMonth(int $server)
    {
        return $this->getTopQuery($server)
            ->where('ems_stat.created_at', '>', (new Carbon())->subMonth()->startOfMonth())
            ->where('ems_stat.created_at', '<=', (new Carbon())->subMonth()->endOfMonth())->get();
    }

    public function currentWeek(int $server)
    {
        return $this->getTopQuery($server)
            ->where('ems_stat.created_at', '>', (new Carbon())->startOfWeek())
            ->where('ems_stat.created_at', '<=', (new Carbon())->endOfWeek())->get();
    }

    public function lastWeek(int $server)
    {
        return $this->getTopQuery($server)
            ->where('ems_stat.created_at', '>', (new Carbon())->subWeek()->startOfWeek())
            ->where('ems_stat.created_at', '<=', (new Carbon())->subWeek()->endOfWeek())->get();
    }

    protected function getTopQuery($server) {
        return (new EMS)->where('ems_stat.server', $server)
            ->leftJoin('members', 'members.name', '=', 'ems_stat.name')
            ->where('members.dismissed', false)
            ->select(['ems_stat.name', \DB::raw('sum(ems_stat.session_time) AS session_time_sum')])
            ->groupBy('ems_stat.name')->orderBy('session_time_sum', 'desc')->limit(10);
    }

    public function hours($type = 'work') {
        $rows = (new EMS())->select('created_at', 'session_time')
            ->where('created_at', '>', (new Carbon())->subMonth())->get();
        $hours = array_fill_keys(range(0, 23), 0);
        foreach ($rows as $row) {
            if ($type === 'work' && $row->created_at->dayOfWeek >= 6) {
                continue;
            } elseif ($type === 'holiday' && $row->created_at->dayOfWeek < 6) {
                continue;
            }
            $session = $row->session_time / 60 / 60;
            for ($i = 0; $i < $session; $i++) {
                $hour = $row->created_at->hour + $i;
                if ($hour > 23) {
                    $hour = $hour - 24;
                }
                if ($i + 1 > $session) {
                    $hours[$hour] += $session - $i;
                } else {
                    $hours[$hour] += 1;
                }
            }
        }
        foreach ($hours as &$hour) {
            if ($type === 'work') {
                $hour = $hour / 21;
            } elseif ($type === 'holiday') {
                $hour = $hour / 9;
            } else {
                $hour = $hour / 30;
            }
        }
        return $hours;
    }
}