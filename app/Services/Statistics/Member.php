<?php namespace App\Services\Statistics;

use App\EMS;
use Carbon\Carbon;

class Member {
    protected $name;
    protected $rank;

    public function __construct(string $name, $rank = false)
    {
        $this->name = $name;
        $this->rank = $rank;
    }

    public function total() {
        $rows = $this->getQuery()->get();
        return $this->response($rows);
    }

    public function currentMonth() {
        $rows = $this->getQuery()->where('created_at', '>=', (new Carbon())->day(1)->hour(1)->second(1))->get();
        return $this->response($rows);
    }

    public function lastMonth() {
        $rows = $this->getQuery()
            ->where('created_at', '<', (new Carbon())->day(1)->hour(1)->second(1))
            ->where('created_at', '>=', (new Carbon())->day(1)->hour(1)->second(1)->subMonth(1))
            ->get();
        return $this->response($rows);
    }

    public function isReadyToUp($rank, $seconds) {
        return isset(\App\Member::$up[$rank]) && \App\Member::$up[$rank] !== false && \App\Member::$up[$rank] < $seconds / 60 / 60;
    }

    public function isRetention($rank, $seconds) {
        return isset(\App\Member::$retention[$rank])&& \App\Member::$retention[$rank] !== false && \App\Member::$retention[$rank] < $seconds / 60 / 60;
    }

    protected function response($rows) {
        $total = 0;
        foreach ($rows as $row) {
            $total += $row->session_time_sum;
        }
        return [
            'servers' => $rows,
            'total' => $total,
            'retention' => $this->rank !== false && $this->isRetention($this->rank, $total),
            'up' =>  $this->rank !== false && $this->isReadyToUp($this->rank, $total),
        ];
    }

    protected function getQuery() {
        return (new EMS)->select(['name', 'server', \DB::raw('sum(session_time) AS session_time_sum')])
            ->where('session_time', '>=', 60)
            ->where('name', $this->name)
            ->groupBy(['name', 'server']);
    }

}