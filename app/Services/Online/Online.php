<?php namespace App\Services\Online;

use App\EMS;
use App\Member;

class Online {

    protected $current = [

    ];

    public function __construct()
    {

    }

    public function create() {
        $members = (new Member())
            ->where('vacation', false)
            ->where('dismissed', false)
            ->pluck('name')->toArray();
        foreach (EMS::$servers as $serverKey => $serverName) {
            $data = file_get_contents('C:\Users\Administrator\AppData\Roaming\TS3Client\plugins\lua_plugin\Modules_EMSCore\Logs\\'.$serverKey.'.log');
            $rows = explode(PHP_EOL, $data);
            natsort($rows);
            $this->current[$serverKey] = [
                'key' => $serverKey,
                'name' =>$serverName,
                'total_all' => 0,
                'total_ems' => 0,
                'items' => [],
            ];
            foreach ($rows as $row) {
                $isEMS = in_array(trim($row), $members) === true;
                $this->current[$serverKey]['items'][] = [
                    'name' => trim($row),
                    'group' => $isEMS ? 'ems' : 'none'
                ];
                $this->current[$serverKey]['total_all']++;
                if ($isEMS) {
                    $this->current[$serverKey]['total_ems']++;
                }
            }
        }
        return $this;
    }

    public function get() {
        return $this->current;
    }

}